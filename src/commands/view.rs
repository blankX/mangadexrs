use crate::utils;

use std::process::exit;
use clap::ArgMatches;
extern crate tokio;
extern crate reqwest;

pub async fn view(arg_m: &ArgMatches<'_>) {
    let client = reqwest::Client::new();
    let languages: Vec<_> = arg_m.values_of("language").unwrap_or_default().collect();
    let handles = arg_m.values_of("id").unwrap().map(|id| {
        let cloned_client = client.clone();
        let id = id.to_string();
        let cid = id.parse().unwrap();
        (tokio::spawn(async move {
            utils::get_manga(cloned_client, cid).await
        }), id)
    }).collect::<Vec<_>>();
    let mut fail = false;
    let mut one_done = false;
    for handle in handles {
        let (handle, id) = handle;
        let manga = match handle.await {
            Ok(manga) => manga,
            Err(err) => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("ID: {}\nError: {}", id, err);
                fail = true;
                one_done = true;
                continue;
            }
        };
        match manga {
            Ok(manga) => {
                match manga {
                    Some(mut manga) => {
                        let mut text = manga.data.manga.to_string();
                        if one_done {
                            text = format!("\n{}", text);
                        }
                        text.push_str("\nChapters:");
                        manga.data.chapters.reverse();
                        for chapter in &manga.data.chapters {
                            if !languages.is_empty() && !languages.contains(&chapter.language.as_str()) {
                                continue;
                            }
                            text.push_str(&format!("\n- {}", chapter));
                            if Some(chapter.chapter.clone()) == manga.data.manga.last_chapter {
                                text.push_str(" [END]");
                            }
                        }
                        println!("{}", text);
                    },
                    None => {
                        if one_done {
                            eprintln!("");
                        }
                        eprintln!("ID: {}\nError: does not exist", id);
                        fail = true;
                    }
                };
            },
            Err(err) => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("ID: {}\nError: {}", id, err);
                fail = true;
            }
        };
        one_done = true;
    }
    if fail {
        exit(1);
    }
}
