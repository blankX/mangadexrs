use crate::utils;

use std::io::Cursor;
use std::process::exit;
use clap::ArgMatches;
use quick_xml::Writer;
use quick_xml::events::{Event, BytesStart, BytesText, BytesEnd};
extern crate reqwest;

pub async fn feed(arg_m: &ArgMatches<'_>) {
    let client = reqwest::Client::new();
    let manga_id = arg_m.value_of("id").unwrap();
    let languages: Vec<_> = arg_m.values_of("language").unwrap_or_default().collect();
    let manga_info = match utils::get_manga(client, manga_id.parse().unwrap()).await {
        Ok(Some(manga_info)) => manga_info,
        Ok(None) => {
            eprintln!("ID: {}\nError: does not exist", &manga_id);
            exit(1);
        },
        Err(err) => {
            eprintln!("ID: {}\nError: {}", &manga_id, err);
            exit(1);
        }
    };
    let mut writer = Writer::new(Cursor::new(Vec::new()));
    {
        let mut elem = BytesStart::owned(b"rss".to_vec(), 3);
        elem.push_attribute(("version", "2.0"));
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"channel".to_vec(), 7);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"title".to_vec(), 5);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&manga_info.data.manga.title).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"title".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesStart::owned(b"link".to_vec(), 4);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&format!("https://mangadex.org/title/{}", &manga_id)).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"link".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesStart::owned(b"description".to_vec(), 11);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&manga_info.data.manga.description).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"description".to_vec());
        writer.write_event(Event::End(elem)).unwrap();
    }

    for chapter in manga_info.data.chapters {
        if !languages.is_empty() && !languages.contains(&chapter.language.as_str()) {
            continue;
        }
        let link = format!("https://mangadex.org/chapter/{}", &chapter.id);
        let elem = BytesStart::owned(b"item".to_vec(), 4);
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesStart::owned(b"title".to_vec(), 5);
        writer.write_event(Event::Start(elem)).unwrap();

        let mut title = chapter.to_string();
        if Some(chapter.chapter.clone()) == manga_info.data.manga.last_chapter {
            title.push_str(" [END]");
        }
        let elem = BytesText::from_plain_str(&title).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"title".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let mut elem = BytesStart::owned(b"guid".to_vec(), 4);
        elem.push_attribute(("isPermaLink", "true"));
        writer.write_event(Event::Start(elem)).unwrap();

        let elem = BytesText::from_plain_str(&link).into_owned();
        writer.write_event(Event::Text(elem)).unwrap();

        let elem = BytesEnd::owned(b"guid".to_vec());
        writer.write_event(Event::End(elem)).unwrap();

        let elem = BytesEnd::owned(b"item".to_vec());
        writer.write_event(Event::End(elem)).unwrap();
    }

    let elem = BytesEnd::owned(b"channel".to_vec());
    writer.write_event(Event::End(elem)).unwrap();

    let elem = BytesEnd::owned(b"rss".to_vec());
    writer.write_event(Event::End(elem)).unwrap();

    println!("{}", String::from_utf8(writer.into_inner().into_inner()).unwrap());
}
