mod commands;
mod structs;
mod utils;
use clap::{App, AppSettings, Arg, SubCommand};
extern crate tokio;

const LANGUAGES: &[&str] = &["sa", "bd", "bg", "mm", "ct", "cn", "hk", "cz", "dk", "nl", "gb", "ph", "fi", "fr", "de", "gr", "il", "in", "hu", "id", "it", "jp", "kr", "lt", "my", "mn", "no", "other", "ir", "pl", "br", "pt", "ro", "ru", "rs", "es", "mx", "se", "th", "tr", "ua", "vn"];

fn validate_int(i: String) -> Result<(), String> {
    match i.parse::<i32>() {
        Ok(_) => Ok(()),
        Err(err) => Err(err.to_string())
    }
}

fn main() {
    let matches = App::new("mangadexrs")
        .about("mangadex.org downloader in rust")
        .version(env!("CARGO_PKG_VERSION"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(
            SubCommand::with_name("view")
            .aliases(&["info", "show"])
            .arg(
                Arg::with_name("language")
                    .long("language")
                    .short("l")
                    .takes_value(true)
                    .multiple(true)
                    .possible_values(LANGUAGES)
                    .help("Only show chapters of the specified languages")
            ).arg(
                Arg::with_name("id")
                    .takes_value(true)
                    .multiple(true)
                    .required(true)
                    .validator(validate_int)
            )
        )
        .subcommand(
            SubCommand::with_name("feed")
            .alias("rss")
            .arg(
                Arg::with_name("language")
                    .long("language")
                    .short("l")
                    .takes_value(true)
                    .multiple(true)
                    .possible_values(LANGUAGES)
                    .help("Only show chapters of the specified languages")
            ).arg(
                Arg::with_name("id")
                    .takes_value(true)
                    .required(true)
                    .validator(validate_int)
            )
        )
        .subcommand(
            SubCommand::with_name("download")
            .alias("dl")
            .arg(
                Arg::with_name("print")
                    .long("print")
                    .short("p")
                    .help("Print the URL to download only")
            ).arg(
                Arg::with_name("manga_ids")
                    .long("manga-ids")
                    .short("m")
                    .takes_value(true)
                    .multiple(true)
                    .required_unless("chapter_ids")
                    .help("Add chapters from the manga specified, limit languages with --language")
            ).arg(
                Arg::with_name("language")
                    .long("language")
                    .short("l")
                    .takes_value(true)
                    .multiple(true)
                    .possible_values(LANGUAGES)
                    .help("Filter languages for manga_ids, has no effect on chapter_ids")
            ).arg(
                Arg::with_name("chapter_ids")
                    .takes_value(true)
                    .required_unless("manga_ids")
                    .multiple(true)
                    .validator(validate_int)
            )
        )
        .get_matches();

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    match matches.subcommand() {
        ("view", Some(sub_m)) => runtime.block_on(commands::view(sub_m)),
        ("feed", Some(sub_m)) => runtime.block_on(commands::feed(sub_m)),
        ("download", Some(sub_m)) => runtime.block_on(commands::download(sub_m)),
        _ => panic!("AppSettings::SubcommandRequiredElseHelp do your job please")
    };
}
